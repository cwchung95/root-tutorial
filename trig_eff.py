import ROOT
from ROOT import TH1F, TCanvas, TChain

ch = TChain("tree")
ch.Add("/data4/public/root-tutorial-merged-norm/SingleMuon*")

h1_den = TH1F("h1_den","h1_den",20,0,2000)
h1_num = TH1F("h1_num","h1_num",20,0,2000)

ch.Draw("ht>>h1_den","1","goff")
ch.Draw("ht>>h1_num","trig_ht900","goff")

h1_eff = h1_num.Clone("h1_eff")
h1_eff.Divide(h1_den)

c = TCanvas("c","c",800,800)

h1_den.SetStats(0)
h1_den.SetLineColor(ROOT.kRed)
h1_den.Draw("hist")
h1_eff.Scale((1/3.)*h1_den.GetMaximum())
h1_eff.Draw("same b")
h1_num.SetLineColor(ROOT.kBlue)
h1_num.Draw("same hist")
c.Print("test.pdf")
